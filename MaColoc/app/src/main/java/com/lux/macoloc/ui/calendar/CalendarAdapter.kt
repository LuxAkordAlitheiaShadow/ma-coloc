package com.lux.macoloc.ui.calendar

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lux.macoloc.R
import java.time.LocalDate

class CalendarAdapter(
    private val days: ArrayList<LocalDate>,
    private val onItemListener: OnItemListener) : RecyclerView.Adapter<CalendarViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CalendarViewHolder {

        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.calendar_cell, parent, false)
        val layoutParams = view.layoutParams
        if (days.size > 15) { // month View
            layoutParams.height = (parent.height * 0.166666666).toInt()
        } else { // week View
            layoutParams.height = parent.height
        }
        return CalendarViewHolder(view, onItemListener, days)
    }

    override fun onBindViewHolder(holder: CalendarViewHolder, position: Int) {
        val date = days[position]
        holder.dayOfMonth.text = date.dayOfMonth.toString()
        if (date == CalendarUtils.selectedDate) {
            holder.parentView.setBackgroundColor(Color.rgb(88,93,220))
            //TODO("CHANGE TO GLOBAL COLOR IN COLOR.XML")
        }
        if (date.month == CalendarUtils.selectedDate!!.month) {
            holder.dayOfMonth.setTextColor(Color.LTGRAY)
        }
        else {
            holder.dayOfMonth.setTextColor(Color.rgb(88,93,220))
        }
    }

    override fun getItemCount(): Int {
        return days.size
    }

    interface OnItemListener {
        fun onItemClick(position: Int, date: LocalDate?)
    }
}