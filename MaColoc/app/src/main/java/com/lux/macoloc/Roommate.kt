package com.lux.macoloc

data class Roommate
(
    val name : String,
    val balance : Double
)