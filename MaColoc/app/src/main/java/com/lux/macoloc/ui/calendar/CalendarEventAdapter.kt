package com.lux.macoloc.ui.calendar

import android.annotation.SuppressLint
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.lux.macoloc.MainActivity
import com.lux.macoloc.R

class CalendarEventAdapter(private val eventList: ArrayList<Calendar>) : RecyclerView.Adapter<CalendarEventAdapter.CalendarViewHolder>()
{
    val database = Firebase.firestore

    class CalendarViewHolder (view: View) : RecyclerView.ViewHolder(view) {
        val eventCellNameTV : TextView = view.findViewById(R.id.eventCellNameTV)
        val eventCellHoursTV : TextView = view.findViewById(R.id.eventCellHoursTV)
        val eventTypeEventTV : TextView = view.findViewById(R.id.eventCellTypeEventTV)
        val eventCellParent: LinearLayout = view.findViewById(R.id.eventCellParent)
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int) : CalendarViewHolder
    {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.calendar_event_cell, viewGroup, false)

        return CalendarViewHolder(view)
    }

    private fun bindDeleteButtonId(deleteEventButton: ImageButton, event: Calendar) {
            deleteEventButton.tag = event.name
    }

    private fun deleteEvent(deleteEventButton: ImageButton) {
        database
            .collection("flatshares")
            .document(MainActivity.flatshareId)
            .collection("calendarEvent")
            .whereEqualTo("name", deleteEventButton.tag)
            .get()
            //.delete()
            .addOnSuccessListener {
                    documents -> Log.d("MAJ", "Event deleted")
                for (event in documents) {
                    database
                        .collection("flatshares")
                        .document(MainActivity.flatshareId)
                        .collection("calendarEvent")
                        .document(event.id)
                        .delete()
                        .addOnSuccessListener {Log.d("MAJ", "Event deleted")}
                        .addOnFailureListener { exception -> Log.d("INFO", "Error deleting documents: ", exception)}
                }
            }
            .addOnFailureListener { exception -> Log.d("INFO", "Id Document not found: ", exception)
            }
    }

    // Replace the contents of a view (invoked by the layout manager)
    @SuppressLint("ResourceAsColor")
    override fun onBindViewHolder(viewHolder: CalendarViewHolder, position: Int)
    {
        val deleteEventButton: ImageButton = viewHolder.itemView.findViewById(R.id.deleteEventButton)

        viewHolder.eventCellNameTV.text = eventList[position].name
        viewHolder.eventCellHoursTV.text = eventList[position].time
        viewHolder.eventTypeEventTV.text = eventList[position].category
        when (eventList[position].category) {
            //TODO ("GET THE DYNAMIC COLORS, DOSEN'T WORK ANYWAY")
            //  "Cours" -> viewHolder.eventCellParent.setBackgroundColor(R.color.lesson)
            //  "Travail" -> viewHolder.eventCellParent.setBackgroundColor(R.color.work)
            //  "Soirée" -> viewHolder.eventCellParent.setBackgroundColor(R.color.party)
            //  "Évènement" -> viewHolder.eventCellParent.setBackgroundColor(R.color.event)
            //  "Autres" -> viewHolder.eventCellParent.setBackgroundColor(R.color.other)
            "Cours" -> viewHolder.eventCellParent.setBackgroundColor(Color.rgb(218,52,52))
            "Travail" -> viewHolder.eventCellParent.setBackgroundColor(Color.rgb(133,192,255))
            "Soirée" -> viewHolder.eventCellParent.setBackgroundColor(Color.rgb(255,133,242))
            "Évènement" -> viewHolder.eventCellParent.setBackgroundColor(Color.rgb(143,147,241))
            "Autres" -> viewHolder.eventCellParent.setBackgroundColor(Color.rgb(3,218,197))
        }

        bindDeleteButtonId(deleteEventButton, eventList[position])
        deleteEventButton.setOnClickListener {
            // TODO: IMPLEMENT ALERT DIALOG
            deleteEvent(deleteEventButton)
            eventList.removeAt(position)
            notifyItemRemoved(position)
        }
    }
    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() : Int
    {
        return eventList.size
    }
}

