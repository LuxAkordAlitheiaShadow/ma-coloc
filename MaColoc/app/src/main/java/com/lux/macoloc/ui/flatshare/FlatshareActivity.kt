package com.lux.macoloc.ui.flatshare

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.FirebaseMessaging
import com.lux.macoloc.*

class FlatshareActivity : AppCompatActivity()
{
    // Attributes.
    private lateinit var firebaseAuth : FirebaseAuth

    val database = Firebase.firestore

    // Methods.

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_flatshare)

        this.finishMainActivity()

        // Setting on click button event
        this.setCreateFlatshareButtonListener()
        this.setJoinFlatshareButtonListener()
    }

    // Setting on click listener on create_flatshare button
    private fun setCreateFlatshareButtonListener()
    {
        findViewById<Button>(R.id.create_flatshare_button)
            .setOnClickListener {
                // Getting new flatshare name
                val flatshareName = findViewById<EditText>(R.id.new_flatshare_name).text.toString()
                // Flatshare name is empty statement
                if ( flatshareName.isEmpty() )
                {
                    // Displaying error message
                    Toast.makeText(this, "Le nom de la colocation est vide", Toast.LENGTH_SHORT).show()
                }
                // Flatshare name is filled statement
                else
                {
                    this.addUserInDatabase(flatshareName, true, null)
                }
            }
    }

    // Setting on click listener on join_flatshare button
    private fun setJoinFlatshareButtonListener()
    {
        findViewById<Button>(R.id.join_flatshare_button)
            .setOnClickListener {
                // Getting flatshare code
                val flatshareCode = findViewById<EditText>(R.id.join_flatshare_code).text.toString()
                // Ftalshare code is empty statement
                if ( flatshareCode.isEmpty() )
                {
                    // Displaying error message
                    Toast.makeText(this, "Le code de colocation est vide", Toast.LENGTH_SHORT).show()
                }
                // Flatshare code is filled statement
                else
                {
                    this.gettingFlatshareFromDatabase(flatshareCode)
                }
            }
    }

    private fun gettingFlatshareFromDatabase(flatshareCode : String)
    {
        // Getting flatshare in database
        database
            .collection("flatshares")
            .document(flatshareCode)
            .get()
            .addOnSuccessListener { flatshareDocument ->
                // Flatshare document exists statement
                if ( flatshareDocument.exists() )
                {
                    this.addUserInDatabase(null, false, flatshareDocument.id)
                    Log.d("Getting flatshare in gettingFlatshareFromDatabase::FlashareActivity","Success getting document with ID $flatshareDocument")
                }
                // Flashare document doesn't exists statement
                else
                {
                    // Displaying error message
                    Toast.makeText(this, "Le code de colocation est incorrect", Toast.LENGTH_SHORT).show()
                }
            }
            .addOnFailureListener { exception ->
                Log.d("Getting flatshare in gettingFlatshareFromDatabase::FlashareActivity", "Error getting documents: ", exception)
            }
    }

    // Adding flatshare to Firestore database
    private fun addFlatshareToDatabase(flatshareName : String)
    {
        database
            .collection("flatshares")
            .add(Flatshare(flatshareName))
            .addOnSuccessListener { flatshareDocument ->
                this.gettingUserFromDatabase(flatshareDocument.id)
            }
            .addOnFailureListener { exception ->
                Log.d("Adding flatshare in addFlatshareToDatabase::FlashareActivity", "Error adding documents: ", exception)
            }
    }

    // Getting user document from database
    private fun gettingUserFromDatabase(flatshareId : String)
    {
        // Getting username
        firebaseAuth = FirebaseAuth.getInstance()
        val username = firebaseAuth.currentUser?.displayName
        // Getting user in Firestore database
        database
            .collection("users")
            .whereEqualTo("username", username)
            .limit(1)
            .get()
            .addOnSuccessListener { userDocument ->
                // Updating flatshare on user document
                this.updateUserFlatshareToDatabase(userDocument, flatshareId)
                for ( user in userDocument )
                {
                    val username = user.data["username"].toString()
                    this.updateRoommateFlatshareToDatabase(flatshareId, username)
                }

            }
            .addOnFailureListener { exception ->
                Log.d("Getting user in gettingUserFromDatabase::FlashareActivity", "Error getting documents: ", exception)
            }
    }

    // Adding new roommate to existing flatshare document
    private fun updateRoommateFlatshareToDatabase(flatshareId : String, username : String)
    {
        database
            .collection("flatshares")
            .document(flatshareId)
            .collection("roommates")
            .add(Roommate(username, 0.0))
            .addOnSuccessListener { documentReference ->
                Log.d("Updating roommate flatshare in updateRoommateFlatshareToDatabase::FlashareActivity", "Success adding document with ID $documentReference")
            }
            .addOnFailureListener { exception ->
                Log.d("Updating roommate flatshare in updateRoommateFlatshareToDatabase::FlashareActivity", "Error updating documents: ", exception)
            }
    }

    // Updating flatshare on user document
    private fun updateUserFlatshareToDatabase(userDocument : QuerySnapshot, flatshareId : String)
    {
        for ( user in userDocument )
        {
            database
                .collection("users")
                .document(user.id)
                .update("flatshare", flatshareId)
                .addOnSuccessListener {
                    Log.d("Updating user flatshare", "Success for $user")
                    this.showMainActivity()
                }
                .addOnFailureListener {
                    Log.d("Updating user flatshare", "Failure for $user")
                }
        }
    }

    // Launching main activity
    private fun showMainActivity()
    {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        this.finish()
    }

    // Adding user in Firestore database
    private fun addUserInDatabase(flatshareData: String?, flatshareCreation: Boolean, flatshareDocumentId: String?)
    {
        // Getting username
        firebaseAuth = FirebaseAuth.getInstance()
        val username = firebaseAuth.currentUser?.displayName

        // Getting user device's FCM token
        FirebaseMessaging.getInstance().token.addOnCompleteListener { task ->
            if (task.isSuccessful)
            {
                val userToken = task.result
                // Adding data in Firestore database
                database
                    .collection("users")
                    .add(User(username, null, userToken))
                    .addOnSuccessListener { documentReference ->
                        Log.d("Adding user in addUserInDatabase::FlatshareActivity", "Success on adding document with ID : $documentReference")
                        // Creating new flatshare statement
                        if ( flatshareCreation )
                        {
                            // Adding flatshare to Firestore database
                            this.addFlatshareToDatabase(flatshareData!!)
                        }
                        // Joining existing flatshare statement
                        else
                        {
                            this.gettingUserFromDatabase(flatshareDocumentId!!)
                        }
                    }
                    .addOnFailureListener { exception ->
                        Log.d("Adding user in addUserInDatabase::FlatshareActivity", "Error getting documents: ", exception)
                    }
            }
            else
            {
                Log.w("ERROR TOKEN", "Fetching FCM registration token failed", task.exception) // TODO change Log
            }
        }
    }

    // Sending broadcast signal to finish Main Activity
    internal fun finishMainActivity()
    {
        val intent = Intent("finish_main_activity")
        sendBroadcast(intent)
    }
}