package com.lux.macoloc.ui.task.form

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.lux.macoloc.R
import com.lux.macoloc.ui.task.Roommates

//Adapter for the recycler of names of people of the add task form page
class TaskAdapter(private val cardList: List<Roommates>) : RecyclerView.Adapter<TaskAdapter.TaskViewHolder>() {

    // Class containing elements of treasury card layout
    class TaskViewHolder(view: View) : RecyclerView.ViewHolder(view)
    {
        val personView : TextView = view.findViewById(R.id.person_name_task_card)
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int) : TaskViewHolder
    {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.task_card, viewGroup, false)

        return TaskViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: TaskViewHolder, position: Int)
    {
        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        viewHolder.personView.text = cardList[position].name
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() : Int
    {
        return cardList.size
    }
}