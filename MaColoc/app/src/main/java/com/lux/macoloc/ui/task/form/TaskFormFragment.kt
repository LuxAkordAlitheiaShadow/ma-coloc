package com.lux.macoloc.ui.task.form

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.checkbox.MaterialCheckBox
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.lux.macoloc.MainActivity
import com.lux.macoloc.R
import com.lux.macoloc.databinding.FragmentTaskFormBinding
import com.lux.macoloc.ui.task.Roommates
import com.lux.macoloc.ui.task.Task

class TaskFormFragment : Fragment()
{
    private var _binding: FragmentTaskFormBinding? = null

    // This property is only valid between onCreateView and onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        _binding = FragmentTaskFormBinding.inflate(inflater, container, false)
        val root: View = binding.root

        // Getting roommates from database
        this.getRoommatesFromDB()

        // Getting roommates from database and create a list of them
        val roommates : java.util.ArrayList<Roommates> = this.getRoommates()

        // Adding click event on addTask button
        this.addListenerFormButton(roommates)

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    // Getting roommates from database and create a list of them
    private fun getRoommates(): ArrayList<Roommates> {
        val roommatesData = ArrayList<Roommates>()
        val database = Firebase.firestore
        database.collection("flatshares")
            .document(MainActivity.flatshareId)
            .collection("roommates")
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    Log.d("Getting roommates", "${document.id} => ${document.data}")
                    roommatesData.add(
                        Roommates(
                            //document.data["id"] as Int,
                            document.data["name"] as String
                        )
                    )
                }
                this.displayRoommates(roommatesData)
            }
            .addOnFailureListener { exception ->
                Log.w("Getting roommates", "Error getting documents: ", exception)
            }
        return roommatesData
    }

    // Getting roommates from database
    private fun getRoommatesFromDB()
    {
        val roommatesData = ArrayList<Roommates>()
        val database = Firebase.firestore
        database.collection("flatshares").document(MainActivity.flatshareId).collection("roommates")
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    Log.d("Getting roommates", "${document.id} => ${document.data}")
                    roommatesData.add(
                        Roommates(
                            //document.data["id"] as Int,
                            document.data["name"] as String
                        )
                    )
                }
                this.displayRoommates(roommatesData)
            }
            .addOnFailureListener { exception ->
                Log.w("Getting roommates", "Error getting documents: ", exception)
            }
    }

    // Displaying task to fragment
    private fun displayRoommates( roommatesData : ArrayList<Roommates> )
    {
        // Getting the recycler view
        val recyclerTask = binding.recyclerTask
        recyclerTask.layoutManager = LinearLayoutManager(requireContext())

        // Setting data to recycler view
        val taskAdapter = TaskAdapter(roommatesData)
        recyclerTask.adapter = taskAdapter
    }

    // Displaying task fragment
    private fun showTaskListingFragment()
    {
        findNavController().navigate(R.id.fragment_task_form_to_fragment_task)
    }

    // Adding redirection event on add_treasury button
    private fun addListenerFormButton(roommates: java.util.ArrayList<Roommates>)
    {
        binding.addTask.setOnClickListener {
            // Add task to db
            val nameField : String = binding.taskFormName.text.toString()
            val roommateNameFieldList : List<String> = this.getCheckedRoommates(roommates)
            Log.d("addTaskForm", "Adding new task to DB")
            this.addTaskToDB(nameField, roommateNameFieldList)
            showTaskListingFragment() // Go back to task listing fragment
        }
    }

    // Getting the checked roommates from the form
    private fun getCheckedRoommates(roommates: java.util.ArrayList<Roommates>) : MutableList<String>
    { //TODO get roommates checked and put them in a list (roommatesAsigned)
        val roommatesAssigned : MutableList<String> = mutableListOf()
        for ((i, _) in roommates.withIndex())
        {
            val roommateHolder = binding.recyclerTask.findViewHolderForAdapterPosition(i)
            val roommateItem = roommateHolder?.itemView
            val roommateCheckbox = roommateItem?.findViewById<MaterialCheckBox>(R.id.checkbox_person_name_task_card)
            val roommateName = roommateItem?.findViewById<TextView>(R.id.person_name_task_card)
            // Roommate is selected
            if (roommateCheckbox?.isChecked == true)
            {
                Log.d("roommateName", "roommateName : " + roommateName?.text.toString())
                roommatesAssigned.add(roommateName?.text.toString())
            }
        }
        return roommatesAssigned
    }

    // Pushing new treasury to database
    private fun addTaskToDB(nameField : String, roommateNameFieldList : List<String>)
    {
        val database = Firebase.firestore
        database.collection("flatshares").document(MainActivity.flatshareId).collection("tasks")
            .add(
                Task(
                nameField,
                roommateNameFieldList
            )
            )
            .addOnSuccessListener { documentReference ->
                Log.d("Add task to DB", "Task added with ID : $documentReference.id")
            }
            .addOnFailureListener { e ->
                Log.d("Add task to DB", "Error adding task to DB", e)
            }
    }

}