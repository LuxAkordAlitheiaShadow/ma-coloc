package com.lux.macoloc.ui.calendar

import java.time.DayOfWeek
import java.time.LocalDate
import java.time.LocalTime
import java.time.YearMonth
import java.time.format.DateTimeFormatter

object CalendarUtils {
    var selectedDate: LocalDate? = null
    fun formattedDate(date: LocalDate?): String {
        val formatter = DateTimeFormatter.ofPattern("dd MMMM yyyy")
        return date!!.format(formatter)
    }

    fun formattedShortDate(date: LocalDate?): String {
        val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
        return date!!.format(formatter)
    }

    fun formattedShortTime(time: LocalTime?): String {
        val formatter = DateTimeFormatter.ofPattern("HH:mm") // Hours, minutes, seconds and pm/am
        return time!!.format(formatter)
    }

    fun monthYearFromDate(date: LocalDate?): String {
        val formatter = DateTimeFormatter.ofPattern("MMMM yyyy")
        return date!!.format(formatter)
    }

    fun daysInMonthArray(): ArrayList<LocalDate> {
        val daysInMonthArray = ArrayList<LocalDate>()
        val yearMonth = YearMonth.from(selectedDate)
        val daysInMonth = yearMonth.lengthOfMonth()
        val prevMonth = selectedDate!!.minusMonths(1)
        val nextMonth = selectedDate!!.plusMonths(1)
        val previousYearMonth = YearMonth.from(prevMonth)
        val previousDaysInMonth = previousYearMonth.lengthOfMonth()
        val firstOfMonth = selectedDate!!.withDayOfMonth(1)
        val dayOfWeek = firstOfMonth.dayOfWeek.value
        for (i in 1..42) {
            when {
                i <= dayOfWeek -> {
                    daysInMonthArray.add(
                        LocalDate.of(
                            prevMonth.year,
                            prevMonth.month,
                            previousDaysInMonth + i - dayOfWeek
                        )
                    )
                }
                i > daysInMonth + dayOfWeek -> {
                    daysInMonthArray.add(
                        LocalDate.of(
                            nextMonth.year,
                            nextMonth.month,
                            i - dayOfWeek - daysInMonth
                        )
                    )
                }
                else -> {
                    daysInMonthArray.add(
                        LocalDate.of(
                            selectedDate!!.year,
                            selectedDate!!.month,
                            i - dayOfWeek
                        )
                    )
                }
            }
        }
        return daysInMonthArray
    }

    fun daysInWeekArray(selectedDate: LocalDate?): ArrayList<LocalDate> {
        val days = ArrayList<LocalDate>()
        var current = sundayForDate(selectedDate)
        val endDate = current!!.plusWeeks(1)
        while (current!!.isBefore(endDate)) {
            days.add(current)
            current = current.plusDays(1)
        }
        return days
    }

    private fun sundayForDate(current: LocalDate?): LocalDate? {
        var current = current
        val oneWeekAgo = current!!.minusWeeks(1)
        while (current!!.isAfter(oneWeekAgo)) {
            if (current.dayOfWeek == DayOfWeek.SUNDAY) {
                return current
            }
            current = current.minusDays(1)
        }
        return null
    }
}