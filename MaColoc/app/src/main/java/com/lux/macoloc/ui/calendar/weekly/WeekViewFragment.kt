package com.lux.macoloc.ui.calendar.weekly

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.lux.macoloc.MainActivity
import com.lux.macoloc.R
import com.lux.macoloc.databinding.FragmentCalendarWeekViewBinding
import com.lux.macoloc.ui.calendar.Calendar
import com.lux.macoloc.ui.calendar.CalendarAdapter
import com.lux.macoloc.ui.calendar.CalendarAdapter.OnItemListener
import com.lux.macoloc.ui.calendar.CalendarEventAdapter
import com.lux.macoloc.ui.calendar.CalendarUtils
import java.time.LocalDate

class WeekViewFragment : Fragment(), OnItemListener {
    // Attributes.
    private var _binding: FragmentCalendarWeekViewBinding? = null
    // This property is only valid between onCreateView and onDestroyView.
    private val binding get() = _binding!!

    val database = Firebase.firestore

    private var monthYearText: TextView? = null
    private var calendarRecyclerView: RecyclerView? = null
    private var eventRecyclerView: RecyclerView? = null
    private var dayOfWeekTV: TextView? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View
    {
        // Creating fragment
        _binding = FragmentCalendarWeekViewBinding.inflate(inflater, container, false)
        val root: View = binding.root

        super.onCreate(savedInstanceState)
        initWidgets()
        CalendarUtils.selectedDate = LocalDate.now()
        setWeekView()
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setEventAdapter()
        val previousWeekAction: ImageButton = view.findViewById(R.id.previousWeekAction)
        previousWeekAction.setOnClickListener {
            previousWeekAction()
        }
        val nextWeekAction: ImageButton = view.findViewById(R.id.nextWeekAction)
        nextWeekAction.setOnClickListener {
            nextWeekAction()
        }
        val newEventAction: Button = view.findViewById(R.id.newEventAction)
        newEventAction.setOnClickListener {
            findNavController().navigate(R.id.fragment_calendar_event_edit)
        }
        val buttonMonthView: Button = view.findViewById(R.id.buttonMonthView)
        buttonMonthView.setOnClickListener {
            findNavController().navigate(R.id.fragment_calendarMonthView)
        }
    }

    private fun initWidgets() {
        calendarRecyclerView = binding.calendarRecyclerView
        monthYearText = binding.monthYearTV
        eventRecyclerView = binding.eventRecyclerView
        dayOfWeekTV = view?.findViewById(R.id.cellDayText)
    }

    private fun setWeekView() {
        monthYearText!!.text =
            CalendarUtils.monthYearFromDate(CalendarUtils.selectedDate)
        val days = CalendarUtils.daysInWeekArray(CalendarUtils.selectedDate)
        val calendarAdapter = CalendarAdapter(days, this)
        val layoutManager: RecyclerView.LayoutManager =
            GridLayoutManager(requireActivity().applicationContext, 7)
        calendarRecyclerView?.layoutManager = layoutManager
        calendarRecyclerView?.adapter = calendarAdapter
        setEventAdapter()
    }

    private fun previousWeekAction() {
        CalendarUtils.selectedDate = CalendarUtils.selectedDate!!.minusWeeks(1)
        setWeekView()
    }

    private fun nextWeekAction() {
        CalendarUtils.selectedDate = CalendarUtils.selectedDate!!.plusWeeks(1)
        setWeekView()
    }

    override fun onItemClick(position: Int, date: LocalDate?) {
        CalendarUtils.selectedDate = date
        setWeekView()
    }

    override fun onResume() {
        super.onResume()
        setEventAdapter()
    }

    private fun setEventsListView( eventsList: ArrayList<Calendar> ) {
        this.setEventView(eventsList)
    }

    private fun setEventView(eventsList: ArrayList<Calendar>) {
        val layoutEventManager: RecyclerView.LayoutManager =
            LinearLayoutManager(requireActivity().applicationContext)
        val calendarEventAdapter = CalendarEventAdapter(eventsList)
        eventRecyclerView?.layoutManager = layoutEventManager
        eventRecyclerView?.adapter = calendarEventAdapter
    }


    // Get all the events
    private fun setEventAdapter() {
        val eventsList = ArrayList<Calendar>()

        database
            .collection("flatshares")
            .document(MainActivity.flatshareId)
            .collection("calendarEvent")
            .whereEqualTo("date", CalendarUtils.selectedDate.toString())
            .get()
            .addOnSuccessListener { documents ->
                for ( event in documents )
                {
                    eventsList.add(Calendar(
                        event.data["name"] as String,
                        event.data["date"] as String,
                        event.data["time"] as String?,
                        event.data["category"] as String?
                    ))
                }
                // Setting form elements
                this.setEventsListView(eventsList)
            }
            .addOnFailureListener { exception ->
                Log.d("INFO", "Error getting documents: ", exception)
            }
    }

}