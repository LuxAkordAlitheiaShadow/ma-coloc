package com.lux.macoloc.ui.treasury.balance

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.lux.macoloc.R

class RoommateBalanceAdapter(private val globalRoommatesCost : Map<String, Double>, private val roommatesNames : ArrayList<String>) : RecyclerView.Adapter<RoommateBalanceAdapter.RoommateBalanceViewHolder>()
{
    // Class containing elements of balance card
    class RoommateBalanceViewHolder(view: View) : RecyclerView.ViewHolder(view)
    {
        val roommateNameTextView : TextView = view.findViewById(R.id.name_balance_card)
        val textView : TextView = view.findViewById(R.id.text_balance_card)
    }

    // Creating new views
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int) : RoommateBalanceViewHolder
    {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.balance_card, viewGroup, false)
        return RoommateBalanceViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: RoommateBalanceViewHolder, position: Int)
    {
        // Getting roommate balance
        val roommateName = roommatesNames[position]
        val roommateBalance : Double? = globalRoommatesCost[ roommateName ]
        // Setting roommate name
        viewHolder.roommateNameTextView.text = roommateName
        // Setting roommate balance
        viewHolder.textView.text = roommateBalance.toString()
    }

    // Returning size of dataset
    override fun getItemCount(): Int
    {
        return globalRoommatesCost.size
    }
}