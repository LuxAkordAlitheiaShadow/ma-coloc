package com.lux.macoloc.ui.treasury.balance

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.lux.macoloc.MainActivity.Companion.flatshareId
import com.lux.macoloc.databinding.FragementTreasuryBalanceBinding

class TreasuryBalanceFragment : Fragment()
{
    // Attributes.
    private var _binding: FragementTreasuryBalanceBinding? = null
    // This property is only valid between onCreateView and onDestroyView.
    private val binding get() = _binding!!

    val database = Firebase.firestore

    // Methods.

    // Method for creating the form fragment
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View
    {
        // Creating fragment
        _binding = FragementTreasuryBalanceBinding.inflate(inflater, container, false)
        val root: View = binding.root
        setHasOptionsMenu(true)

        // Getting and displaying balance
        this.getBalanceAndDisplay()

        // Setting on click listener on sync_balance button
        this.setOnClickSyncBalanceButton()

        return root
    }

    // Fragment's destroyer method.
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    // Method getting roommates balance
    private fun getBalanceAndDisplay()
    {
        // Declaring dataset
        val globalRoommateCost = mutableMapOf<String, Double>()
        val roommatesNames = ArrayList<String>()
        // Getting roommates
        database
            .collection("flatshares")
            .document(flatshareId)
            .collection("roommates")
            .get()
            .addOnSuccessListener { documents ->
                for ( roommate in documents )
                {
                    // Putting roommate balance in map
                    val roommateBalance : Double = roommate.data["balance"] as Double
                    globalRoommateCost[roommate.data["name"] as String] = roommateBalance
                    roommatesNames.add(roommate.data["name"] as String)
                }
                // Displaying data
                this.displayBalanceData(globalRoommateCost, roommatesNames)

            }
            .addOnFailureListener { execption ->
                Log.d("Getting roommate", "Cannot fetch roommate", execption)
            }
    }

    private fun displayBalanceData(globalRoommateCost : Map<String, Double>, roommatesNames : ArrayList<String>)
    {
        // Getting the recycler view
        val roommateBalanceRecyclerView = binding.roommateBalanceRecyclerview
        roommateBalanceRecyclerView.layoutManager = LinearLayoutManager(requireContext())

        // Setting data to recycler view
        val roommateBalanceAdapter = RoommateBalanceAdapter(globalRoommateCost, roommatesNames)
        roommateBalanceRecyclerView.adapter = roommateBalanceAdapter
    }

    // Getting roommate from Firestore database to applying balance synchronisation
    private fun getRoommateFromDatabase()
    {
        val globalStakeholdersCost = mutableMapOf<String, Double?>()
        database
            .collection("flatshares")
            .document(flatshareId)
            .collection("roommates")
            .get()
            .addOnSuccessListener { roommatesDocuments ->
                // Initializing roommates balance
                for (roommate in roommatesDocuments) {
                    globalStakeholdersCost[roommate.data["name"] as String] = 0.0
                }
                this.getTreasuriesFromDatabase(globalStakeholdersCost, roommatesDocuments)
            }
            .addOnFailureListener { exception ->
                Log.w("Getting roommates in getRoommateFromDatabase::TreasuryBalanceFragment", "Error getting documents: ", exception)
            }
    }

    private fun getTreasuriesFromDatabase(globalStakeholdersCost : MutableMap<String, Double?>, roommatesDocuments : QuerySnapshot)
    {
        database
            .collection("flatshares")
            .document(flatshareId)
            .collection("treasuries")
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents)
                {
                    Log.d("Getting treasuries in getTreasuriesFromDatabase::TreasuryBalanceFragment", "${document.id} => ${document.data}")
                    val stakeholdersCost : Map<String, Double> = document.data["stakeholdersCost"] as Map<String, Double>
                    for ( stakeholder in stakeholdersCost.keys )
                    {
                        // Appending current treasury expense balance to the global balance
                        val roundedGlobalStakeholdersCost : Double = String
                            .format("%.2f", globalStakeholdersCost[stakeholder]?.plus(stakeholdersCost[stakeholder]!!))
                            .replace(',','.')
                            .toDouble()
                        globalStakeholdersCost[stakeholder] = roundedGlobalStakeholdersCost
                    }
                }
                this.updateRoommateBalanceToDatabase(globalStakeholdersCost, roommatesDocuments)
            }
            .addOnFailureListener { exception ->
                Log.w("Getting treasuries in getTreasuriesFromDatabase::TreasuryBalanceFragment", "Error getting documents: ", exception)
            }
    }

    private fun updateRoommateBalanceToDatabase(globalStakeholdersCost: Map<String, Double?>, roommatesDocuments: QuerySnapshot)
    {
        for ( roommate in roommatesDocuments ) {
            database
                .collection("flatshares")
                .document(flatshareId)
                .collection("roommates")
                .document(roommate.id)
                .update("balance", globalStakeholdersCost[roommate.data["name"]]?.toDouble())
                .addOnSuccessListener {
                    Log.d("Update balance", "Balance updated for $roommate")
                    this.getBalanceAndDisplay()
                }
                .addOnFailureListener {
                    Log.d("Update balance", "Failed to update balance for $roommate")
                }
        }
    }

    // Adding on click listener on sync_balance button
    private fun setOnClickSyncBalanceButton()
    {
        binding.syncBalanceButton.setOnClickListener {
            this.getRoommateFromDatabase()
            this.getBalanceAndDisplay()
        }
    }
}