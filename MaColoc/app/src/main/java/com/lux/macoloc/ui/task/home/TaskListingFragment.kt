package com.lux.macoloc.ui.task.home

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.lux.macoloc.MainActivity
import com.lux.macoloc.R
import com.lux.macoloc.databinding.FragmentTaskListingBinding
import com.lux.macoloc.ui.task.Task

class TaskListingFragment : Fragment() {
    private var _binding: FragmentTaskListingBinding? = null

    // This property is only valid between onCreateView and onDestroyView.
    private val binding get() = _binding!!
    private val database = Firebase.firestore

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View
    {
        _binding = FragmentTaskListingBinding.inflate(inflater, container, false)
        val root: View = binding.root

        // Getting task from database
        this.getTaskFromDB()

        // Adding click event on addTask button
        this.addListenerFormButton()
        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    // Displaying task form fragment
    private fun showTaskFormFragment()
    {
        findNavController().navigate(R.id.fragment_task_to_fragment_task_form)
    }

    // Getting treasuries from database
    private fun getTaskFromDB()
    {
        val taskData = ArrayList<Task>()
        database
            .collection("flatshares")
            .document(MainActivity.flatshareId)
            .collection("tasks")
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    Log.d("Getting tasks", "${document.id} => ${document.data}")
                    taskData.add(
                        Task(
                        //document.data["id"] as Long,
                        document.data["name"] as String,
                        document.data["person"] as List<String>,
                        )
                    )
                }
                this.displayTask(taskData)
            }
            .addOnFailureListener { exception ->
                Log.w("Getting task", "Error getting documents: ", exception)
            }
    }

    // Displaying task to fragment
    private fun displayTask(taskData : ArrayList<Task> )
    {
        // Getting the recycler view
        val recyclerTask = binding.recyclerTaskListing
        recyclerTask.layoutManager = LinearLayoutManager(requireContext())

        // Setting data to recycler view
        val taskListingAdapter = TaskListingAdapter(taskData, activity as MainActivity)
        recyclerTask.adapter = taskListingAdapter
    }

    // Adding redirection event on add_treasury button
    private fun addListenerFormButton()
    {
        binding.addTask.setOnClickListener {
            Log.d("addTask", "Adding new task card")
            showTaskFormFragment()
        }
    }
}