package com.lux.macoloc.ui.treasury.home

import android.graphics.BitmapFactory
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import com.lux.macoloc.R
import com.lux.macoloc.ui.treasury.Treasury
import java.io.File

class RvCardAdapter(private val cardList: List<Treasury>) : RecyclerView.Adapter<RvCardAdapter.TreasuryViewHolder>()
{
    // Class containing elements of treasury card layout
    class TreasuryViewHolder(view: View) : RecyclerView.ViewHolder(view)
    {
        val libelleView : TextView = view.findViewById(R.id.libelle_treasury_card)
        val costView : TextView = view.findViewById(R.id.cost_treasury_card)
        val buyerView : TextView = view.findViewById(R.id.buyer_treasury_card)
        val stakeholdersView : TextView = view.findViewById(R.id.stakeholders_treasury_card)
        val pictureView : ImageView = view.findViewById(R.id.picture_treasury_card)
        val dateView : TextView = view.findViewById(R.id.date_treasury_card)
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int) : TreasuryViewHolder
    {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.treasury_card, viewGroup, false)

        return TreasuryViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: TreasuryViewHolder, position: Int)
    {
        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        viewHolder.libelleView.text = cardList[position].libelle
        viewHolder.costView.text = cardList[position].cost.toString()
        viewHolder.buyerView.text = cardList[position].buyer

        // Making better text for display stakeholders
        var stakeholdersText : String = ""
        for ( stakeholder in cardList[position].stakeholders)
        {
            stakeholdersText += " $stakeholder,"
        }
        stakeholdersText = stakeholdersText.drop(1)
        stakeholdersText = stakeholdersText.dropLast(1)
        viewHolder.stakeholdersView.text = stakeholdersText//cardList[position].stakeholders.toString()

        viewHolder.dateView.text = cardList[position].date
        // Treasury have proof statement
        if ( cardList[position].picturePath != null )
        {
          this.setPictureFromDB(viewHolder, position)
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() : Int
    {
        return cardList.size
    }

    // Setting picture to treasury's picture view from data in storage
    private fun setPictureFromDB(viewHolder: TreasuryViewHolder, position: Int)
    {
        Log.d("Getting proof picture", "Begging")
        // Storage connection
        val storage = Firebase.storage
        val storageReference = storage.reference
        val picturePathReference = storageReference.child(cardList[position].picturePath.toString())
        // Getting image
        val localTempPictureFile = File.createTempFile("tmpProofPicture","jpg")
        picturePathReference.getFile(localTempPictureFile)
            .addOnSuccessListener {
                Log.d("Get proof", "Success")
                val bitmapPicture = BitmapFactory.decodeFile(localTempPictureFile.absolutePath)
                viewHolder.pictureView.setImageBitmap(bitmapPicture)
            }
            .addOnFailureListener {
                Log.d("Get proof", "Fail to fetch $picturePathReference")
            }
    }
}