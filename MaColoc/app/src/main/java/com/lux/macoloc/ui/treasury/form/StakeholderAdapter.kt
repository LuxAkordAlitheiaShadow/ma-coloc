package com.lux.macoloc.ui.treasury.form

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.lux.macoloc.R
import com.lux.macoloc.Roommate

class StakeholderAdapter(private val roommates: ArrayList<Roommate>) : RecyclerView.Adapter<StakeholderAdapter.StakeholderViewHolder>()
{
    // Class containing elements of stakeholders layout
    class StakeholderViewHolder(view: View) : RecyclerView.ViewHolder(view)
    {
        val stakeholderNameTextView : TextView = view.findViewById(R.id.stakeholder_name)
        val stakeholderCheckBox : CheckBox = view.findViewById(R.id.stakeholder_checkbox)
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int) : StakeholderViewHolder
    {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.stakeholders, viewGroup, false)
        return StakeholderViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: StakeholderViewHolder, position: Int)
    {
        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        val stakeholder = roommates[position]
        Log.d("addRoommateCheckbox", "Add Roommate Checkbox " + stakeholder.name)
        viewHolder.stakeholderNameTextView.text = stakeholder.name
        viewHolder.stakeholderCheckBox.isChecked = false
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() : Int
    {
        return roommates.size
    }
}