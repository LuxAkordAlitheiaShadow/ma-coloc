package com.lux.macoloc.ui.flatshare

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.QueryDocumentSnapshot
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.lux.macoloc.MainActivity
import com.lux.macoloc.R

class RoommatesAdapter(private var roommates : MutableList<String>, private val activity: FlatshareSettingsActivity) : RecyclerView.Adapter<RoommatesAdapter.RoommatesViewHolder>()
{
    // Attributes.
    private lateinit var firebaseAuth: FirebaseAuth
    private val database = Firebase.firestore

    // Class containing elements of roommates layout
    class RoommatesViewHolder(view: View) : RecyclerView.ViewHolder(view)
    {
        val roommatesName: TextView = view.findViewById<TextView>(R.id.roommate_name)
        val expulseRoommateButton: Button = view.findViewById<Button>(R.id.expulse_roommate_button)
    }

    // Methods.

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int) : RoommatesViewHolder
    {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.roommates, viewGroup, false)

        return RoommatesViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: RoommatesViewHolder, position: Int)
    {
        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        viewHolder.roommatesName.text = roommates[position]
        this.setExpulseRoommateButton(viewHolder, position)

    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() : Int
    {
        return roommates.size
    }

    private fun setExpulseRoommateButton(viewHolder: RoommatesViewHolder, position: Int)
    {
        viewHolder.expulseRoommateButton
            .setOnClickListener {
                val usernameToExpulse : String = viewHolder.roommatesName.text.toString()
                // Displaying expulsed roommate balance
                this.checkRoommateBalance(usernameToExpulse)
                // Remove roommate in database
                this.getRoommateFromFlatshareInDatabase(usernameToExpulse)
                // Remove roommate in recycler view
                this.roommates.removeAt(position)

                notifyItemRemoved(position)
            }
    }

    private fun getUserFromUsersInDatabase(usernameToExpulse : String)
    {
        // Getting user in Firestore database
        database
            .collection("users")
            .whereEqualTo("username", usernameToExpulse)
            .limit(1)
            .get()
            .addOnSuccessListener { userDocument ->
                for ( user in userDocument )
                {
                    // Deleting user in users
                    this.deleteUserFromUsersInDatabase(user, usernameToExpulse)
                }
            }
            .addOnFailureListener { exception ->
                Log.d("Getting user in gettingUserFromDatabase::RoommatesAdapter", "Error getting documents: ", exception)
            }
    }

    // Deleting user in users
    private fun deleteUserFromUsersInDatabase(user : QueryDocumentSnapshot, usernameToExpulse : String)
    {
        database
            .collection("users")
            .document(user.id)
            .delete()
            .addOnSuccessListener {
                // Self expulsion statement
                if ( usernameToExpulse == MainActivity.connectedUsername)
                {
                    firebaseAuth = FirebaseAuth.getInstance()
                    activity.finishMainActivity()
                    activity.showFlatshareActivity()
                    activity.finish()
                }
            }
    }

    private fun getRoommateFromFlatshareInDatabase(usernameToExpulse: String)
    {
        // Getting roommate document to delete from flatshare
        database
            .collection("flatshares")
            .document(MainActivity.flatshareId)
            .collection("roommates")
            .whereEqualTo("name", usernameToExpulse)
            .limit(1)
            .get()
            .addOnSuccessListener { roommateDocument ->
                for (roommate in roommateDocument)
                {
                    // Deleting roommate document from flatshares
                    this.deleteRoommateFromFlatshareInDatabase(roommate, usernameToExpulse)
                }
            }
    }

    // Deleting user in flatshares
    private fun deleteRoommateFromFlatshareInDatabase(roommate : QueryDocumentSnapshot, usernameToExpulse: String)
    {
        database
            .collection("flatshares")
            .document(MainActivity.flatshareId)
            .collection("roommates")
            .document(roommate.id)
            .delete()
            .addOnSuccessListener {
                this.getUserFromUsersInDatabase(usernameToExpulse)
            }
    }

    // Displaying expulsed roommate balance
    private fun checkRoommateBalance(usernameToExpulse: String)
    {
        database
            .collection("flatshares")
            .document(MainActivity.flatshareId)
            .collection("roommates")
            .whereEqualTo("name", usernameToExpulse)
            .limit(1)
            .get()
            .addOnSuccessListener { roommateDocument ->
                for ( roommate in roommateDocument )
                {
                    val roommateBalance : Double = roommate.data["balance"] as Double
                    Toast
                        .makeText(
                            activity.applicationContext,
                            "$usernameToExpulse quitte la colocation avec un solde de $roommateBalance $.",
                            Toast.LENGTH_SHORT
                        )
                        .show()
                }
            }
    }
}