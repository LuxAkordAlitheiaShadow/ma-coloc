package com.lux.macoloc.ui.calendar

data class Calendar(
    val name: String,
    val date: String,
    val time: String?,
    val category: String?
)