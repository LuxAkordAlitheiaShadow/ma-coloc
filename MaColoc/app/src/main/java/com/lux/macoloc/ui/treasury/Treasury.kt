package com.lux.macoloc.ui.treasury

data class Treasury
(
    val libelle : String,
    val picturePath : String?,
    val cost : Double,
    val date : String,
    val buyer : String,
    val stakeholders : List<String>,
    val stakeholdersCost : Map<String, Double>
)