package com.lux.macoloc.ui.treasury.form

import android.app.Activity.RESULT_OK
import android.app.DatePickerDialog
import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.checkbox.MaterialCheckBox
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import com.lux.macoloc.MainActivity.Companion.connectedUsername
import com.lux.macoloc.MainActivity.Companion.flatshareId
import com.lux.macoloc.R
import com.lux.macoloc.Roommate
import com.lux.macoloc.databinding.FragmentTreasuryFormBinding
import com.lux.macoloc.ui.treasury.Treasury
import java.io.ByteArrayOutputStream
import java.util.*

class TreasuryFormFragment : Fragment()
{
    // Attributes.
    private var _binding: FragmentTreasuryFormBinding? = null
    // This property is only valid between onCreateView and onDestroyView.
    private val binding get() = _binding!!

    private val database = Firebase.firestore

    // Methods.

    // Method for creating the form fragment
    override fun onCreateView( inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View
    {
        // Creating fragment
        _binding = FragmentTreasuryFormBinding.inflate(inflater, container, false)
        val root: View = binding.root
        setHasOptionsMenu(true)

        // Getting roommate for setting form
        this.getRoommatesAndApplyThem()

        return root
    }

    // Fragment's destroyer method.
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    // Method getting the list of roommates and applying them in fields
    private fun getRoommatesAndApplyThem()
    {
        // Getting roommates
        val roommates : ArrayList<Roommate> = ArrayList()
        database
            .collection("flatshares")
            .document(flatshareId)
            .collection("roommates")
            .get()
            .addOnSuccessListener { documents ->
                for ( roommate in documents )
                {
                    Log.d("Getting roommate : ", roommate.data["name"].toString())
                    roommates.add(Roommate(roommate.data["name"] as String, roommate.data["balance"] as Double))
                }
                // Setting form elements
                this.setFormElement(roommates)
            }
            .addOnFailureListener { exception ->
                Log.d("Getting roommates in getRoommatesAndApplyThem::TreasuryFormFragment", "Error getting documents: ", exception)
            }
    }

    // Method parsing list of roommate as spinner's items
    private fun setBuyers( roommates : ArrayList<Roommate> )
    {
        val buyerSpinner = binding.treasuryFormBuyerSpinner
        val spinnerAdapter : ArrayAdapter<String> = ArrayAdapter<String>(requireContext(), R.layout.buyer_spinner, roommates.map { it.name })
        buyerSpinner.adapter = spinnerAdapter
        buyerSpinner.setSelection(spinnerAdapter.getPosition(connectedUsername))
    }

    // Method making an event listener for prompt a date picker on the date's edit text
    private fun setDateButton()
    {
        val cal = Calendar.getInstance()
        val year = cal.get(Calendar.YEAR)
        val month = cal.get(Calendar.MONTH)
        val day = cal.get(Calendar.DAY_OF_MONTH)
        binding.treasuryFormDate.setOnClickListener {
            val datePickerDialog = DatePickerDialog(requireContext(),
                { _, myear, mmonth, mdayOfMonth ->
                    val fixedMonth = mmonth + 1
                    binding.treasuryFormDate.setText(getString(R.string.edit_form_date, myear, fixedMonth, mdayOfMonth))
                }, year, month, day)
            datePickerDialog.show()
        }
    }

    // Method listing the roommates with checkboxes
    private fun setStakeholderCheckboxes(roommates: ArrayList<Roommate> )
    {
        // Getting stakeholder recycler view
        val stakeholderRecyclerView : RecyclerView = binding.stakeholdersRecyclerView
        stakeholderRecyclerView.layoutManager = LinearLayoutManager(requireContext())
        // Initializing data in recycler view
        val stakeholderAdapter = StakeholderAdapter(roommates)
        stakeholderRecyclerView.adapter = stakeholderAdapter
    }

    // Displaying treasury form fragment
    private fun showTreasuryFragment()
    {
        findNavController().navigate(R.id.fragment_treasury_form_to_fragment_treasury)
    }

    // Setting event click listener on add treasury form button
    private fun setTreasuryButtonListener(roommates: ArrayList<Roommate>)
    {
        // Adding on click listener to button
        binding.addTreasuryFormButton.setOnClickListener {

            // Form is complete statement
            if ( this.validateForm(roommates) )
            {
                // Getting fields value
                val libelleField : String = binding.treasuryFormLibelle.text.toString()

                val costField : String = binding.treasuryFormCost.text.toString()
                // Rounding cost value with 2 decimal
                var costValue : Double = costField.toDouble()
                var roundedCostValue : String = String.format("%.2f", costValue)
                roundedCostValue = roundedCostValue.replace(',', '.')
                costValue = roundedCostValue.toDouble()

                val dateField : String = binding.treasuryFormDate.text.toString()

                val pictureField = binding.proofTreasuryImage

                val spinnerField : String = binding.treasuryFormBuyerSpinner.selectedItem.toString()

                val stakeholders : MutableList<String> = this.getCheckedStakeholders(roommates)

                val stakeholdersCost = this.calculateStakeholdersCost(costValue, spinnerField, stakeholders)

                // Setting picture path for storage
                var pathNamePicture: String? = null
                // Form contain proof statement
                if (pictureField.drawable != null) {
                    val reformatedDate = dateField.replace("/", "-")
                    pathNamePicture = "proof/$flatshareId/$libelleField-$reformatedDate.jpg"
                    val newTreasury : Treasury = Treasury(libelleField, pathNamePicture, costValue, dateField, spinnerField, stakeholders, stakeholdersCost)
                    this.uploadPictureToStorage(newTreasury)
                }
                // From doesn't contain proof statement
                else
                {
                    val newTreasury : Treasury = Treasury(libelleField, pathNamePicture, costValue, dateField, spinnerField, stakeholders, stakeholdersCost)
                    // Pushing new treasury to database
                    this.addTreasuryToDB(newTreasury)
                }
            }
        }
    }

    // Getting the checked stakeholders from the form
    private fun getCheckedStakeholders(roommates: ArrayList<Roommate>) : MutableList<String>
    {
        val stakeholders : MutableList<String> = mutableListOf()
        for ((roommateId, _) in roommates.withIndex())
        {
            val stakeholderHolder = binding.stakeholdersRecyclerView.findViewHolderForAdapterPosition(roommateId)
            val stakeholderItem = stakeholderHolder?.itemView
            val stakeholderCheckbox = stakeholderItem?.findViewById<MaterialCheckBox>(R.id.stakeholder_checkbox)
            val stakeholderName = stakeholderItem?.findViewById<TextView>(R.id.stakeholder_name)
            // Roommate is selected (is a stakeholder)
            if (stakeholderCheckbox?.isChecked == true)
            {
                stakeholders.add(stakeholderName?.text.toString())
            }
        }

        return stakeholders
    }

    // Calculating treasury balance
    private fun calculateStakeholdersCost(cost: Double, buyer: String, stakeholders: List<String>) : Map<String, Double>
    {
        val stakeholdersCost = mutableMapOf<String, Double>()
        val splitCost = cost / stakeholders.size
        for ( stakeholder in stakeholders )
        {
            // Stakeholder isn't the buyer statement
            if ( stakeholder != buyer )
            {
                stakeholdersCost[stakeholder] = splitCost * -1
            }
        }
        // Buyer isn't a stakeholder statement
        if ( buyer !in stakeholders )
        {
            stakeholdersCost[buyer] = cost
        }
        // Buyer is a stakeholder statement
        else
        {
            stakeholdersCost[buyer] = splitCost * (stakeholders.size - 1)
        }

        return stakeholdersCost
    }

    // Pushing new treasury to database
    private fun addTreasuryToDB(newTreasury: Treasury)
    {
        // Add treasury to database
        database
            .collection("flatshares")
            .document(flatshareId)
            .collection("treasuries")
            .add(newTreasury)
            .addOnSuccessListener { documentReference ->
                Log.d("Add treasury to DB", "Treasury added with ID : $documentReference.id")
                // Going to treasury fragment
                this.showTreasuryFragment()
            }
            .addOnFailureListener { e ->
                Log.d("Add treasury to DB", "Error adding treasury to DB", e)
                Toast.makeText(requireContext(), "Impossible de creer une nouvelle depense, veuillez reessayer plus tard.", Toast.LENGTH_SHORT).show()
            }
    }

    // Calling image capture intent
    @Deprecated("Deprecated in Java")
    private fun takePictureIntent()
    {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        try
        {
            startActivityForResult(takePictureIntent, 1)
        }
        catch (e : ActivityNotFoundException)
        {
            Log.d("Take picture", "Activity Not Found")
        }
    }

    // Setting form image view with captured image
    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val REQUEST_IMAGE_CAPTURE = 1
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            val imageBitmap = data?.extras?.get("data") as Bitmap
            val imageView = binding.proofTreasuryImage
            imageView.setImageBitmap(imageBitmap)
        }
    }

    // Adding proof to storage cloud
    private fun uploadPictureToStorage(newTreasury: Treasury)
    {
        val storage = Firebase.storage
        val storageReference = storage.reference

        val proofReference = storageReference.child(newTreasury.picturePath!!)
        val bitmapPicture =
            (binding.proofTreasuryImage.drawable as android.graphics.drawable.BitmapDrawable).bitmap
        val baos = ByteArrayOutputStream()
        bitmapPicture.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val dataPicture = baos.toByteArray()

        val uploadTask = proofReference.putBytes(dataPicture)
        uploadTask
            .addOnFailureListener {
                Log.d("Upload proof", "Failure")
                Toast.makeText(
                    requireContext(),
                    "Impossible de creer une nouvelle depense, veuillez reessayer plus tard.",
                    Toast.LENGTH_SHORT
                ).show()
                this.showTreasuryFragment()
            }
            .addOnSuccessListener {
                Log.d("Upload proof", "Success")
                // Pushing new treasury to database
                this.addTreasuryToDB(newTreasury)
            }
    }

    // Setting event listener on picture button
    private fun setPictureButtonListener()
    {
        val takePictureButton : Button = binding.takePictureButton
        takePictureButton.setOnClickListener {
            this.takePictureIntent()
        }
    }

    // Setting form elements
    private fun setFormElement( roommates: ArrayList<Roommate> )
    {
        // Setting the buyers
        this.setBuyers(roommates)
        // Setting stakeholders checkboxes
        this.setStakeholderCheckboxes(roommates)
        // Adding click event on add_treasury_form button
        this.setTreasuryButtonListener(roommates)
        // Setting date event on the date's edit text
        this.setDateButton()
        // Adding click event on take_picture_button
        this.setPictureButtonListener()
    }

    private fun validateForm(roommates: ArrayList<Roommate>) : Boolean
    {
        // Getting fields value
        val libelleField : String = binding.treasuryFormLibelle.text.toString()
        val costField : String = binding.treasuryFormCost.text.toString()
        val dateField : String = binding.treasuryFormDate.text.toString()
        val stakeholders : MutableList<String> = this.getCheckedStakeholders(roommates)

        // Libelle field is empty statement
        if ( libelleField.isEmpty() )
        {
            Toast.makeText(requireContext(), "Veuillez renseigner le libelle.", Toast.LENGTH_SHORT).show()
            return false
        }
        // Cost field is empty statement
        if ( costField.isEmpty() )
        {
            Toast.makeText(requireContext(), "Veuillez renseigner le cout.", Toast.LENGTH_SHORT).show()
            return false
        }
        // Date field is empty statement
        if ( dateField.isEmpty() )
        {
            Toast.makeText(requireContext(), "Veuillez renseigner la date.", Toast.LENGTH_SHORT).show()
            return false
        }
        // No stakeholders checked statement
        if ( stakeholders.isEmpty() )
        {
            Toast.makeText(requireContext(), "Veuillez cocher les parties prenantes.", Toast.LENGTH_SHORT).show()
            return false
        }
        return true
    }
}
