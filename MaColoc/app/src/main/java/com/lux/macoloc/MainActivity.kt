package com.lux.macoloc

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.PopupMenu
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.FirebaseAuthUIActivityResultContract
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.FirebaseMessaging
import com.lux.macoloc.databinding.ActivityMainBinding
import com.lux.macoloc.ui.flatshare.FlatshareActivity
import com.lux.macoloc.ui.flatshare.FlatshareSettingsActivity

class MainActivity : AppCompatActivity(), PopupMenu.OnMenuItemClickListener {

    private lateinit var binding: ActivityMainBinding
    private lateinit var firebaseAuth : FirebaseAuth
    private lateinit var listener : FirebaseAuth.AuthStateListener
    private lateinit var providers : List<AuthUI.IdpConfig>

    private val database = Firebase.firestore
    private val signInLauncher = registerForActivityResult( FirebaseAuthUIActivityResultContract() ) {}

    companion object
    {
        var flatshareId : String = ""
        var connectedUsername : String? = ""
    }

    override fun onStart() {
        super.onStart()
        firebaseAuth.addAuthStateListener(listener)
    }

    override fun onStop() {
        firebaseAuth.removeAuthStateListener(listener)
        super.onStop()
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        this.initAuthenticator()

        // Opening a broadcast receiver for kill current activity
        val broadcastReciever = object : BroadcastReceiver() {

            override fun onReceive(arg0: Context, intent: Intent) {
                val action = intent.action
                if (action == "finish_main_activity")
                {
                    finish()
                }
            }
        }
        registerReceiver(broadcastReciever, IntentFilter("finish_main_activity"))
    }

    private fun initAuthenticator() {
        providers = arrayListOf(
            AuthUI.IdpConfig.GoogleBuilder().build(),
            AuthUI.IdpConfig.EmailBuilder().build()
        )

        firebaseAuth = FirebaseAuth.getInstance()
        listener = FirebaseAuth.AuthStateListener { p0 ->
            val user = p0.currentUser
            if(user != null)
            {
                this.getUserInDatabase()
            }
            else
            {
                // Making sign in intent
                val signInIntent = AuthUI.getInstance()
                    .createSignInIntentBuilder()
                    .setAvailableProviders(providers)
                    .setTheme(R.style.LoginTheme)
                    .build()
                // Lauching sign in intent
                signInLauncher.launch(signInIntent)
            }
        }
    }

    // Adding on click listener on setting button for showing settings menu
    private fun addListenerSettingButton()
    {
        // Getting button
        val settingButton : Button = findViewById(R.id.setting_button)
        // Setting on click listener
        settingButton.setOnClickListener {
            // Getting menu
            val popupMenu : PopupMenu = PopupMenu(this, it)
            val menuInflater : MenuInflater = popupMenu.menuInflater
            menuInflater.inflate(R.menu.setting_menu, popupMenu.menu)
            // Setting action on menu item
            popupMenu.setOnMenuItemClickListener(this@MainActivity)
            // Showing menu
            popupMenu.show()
        }
    }

    // Setting action on menu item
    override fun onMenuItemClick(item: MenuItem) : Boolean
    {
        return when (item.itemId)
        {
            // Setting logout item action
            R.id.setting_item_logout ->
            {
                // Logging out user
                this.firebaseAuth.signOut()
                true
            }
            // Setting share flatshare code item action
            R.id.setting_item_share_flatshare ->
            {
                // Lauching sharing intent with flatshare ID
                this.sharingFlashareCodeIntent()
                true
            }
            // Setting flatshare setting item action
            R.id.setting_item_flatshare_setting ->
            {
                this.showFlatshareSettingActivity()
                true
            }
            // Setting roommate setting item action
            R.id.setting_item_roommate ->
            {
                Toast
                    .makeText(
                        this,
                        "Paramettres du colocataire pas encore disponible.",
                        Toast.LENGTH_SHORT
                    )
                    .show()
                true
            }
            else -> false
        }
    }

    // Setting views
    private fun setViews()
    {
        // Setting navigation view
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment_activity_main) as NavHostFragment
        val navController: NavController = navHostFragment.navController
        val navView: BottomNavigationView = binding.navView
        navView.setupWithNavController(navController)

        // Adding on click listener on setting button
        this.addListenerSettingButton()
    }

    // Launching flatshare activity
    private fun showFlatshareActivity()
    {
        val intent = Intent(this, FlatshareActivity::class.java)
        startActivity(intent)
    }

    // Launching flatshare setting activity
    private fun showFlatshareSettingActivity()
    {
        val intent = Intent(this, FlatshareSettingsActivity::class.java)
        startActivity(intent)
    }

    // Launching sharing intent with flatshare ID
    private fun sharingFlashareCodeIntent()
    {
        // Setting sharing intent
        val shareIntent : Intent = Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, "Voici le code de la colocation a rentrer dans l'application Ma Coloc : $flatshareId")
            type = "text/plain"
        }
        // Launching sharing intent
        startActivity(shareIntent)
    }

    // Getting flatshare name from database to apply it on header
    private fun getFlatshareNameInDatabase()
    {
        database
            .collection("flatshares")
            .document(flatshareId)
            .get()
            .addOnSuccessListener { flatshareDocument ->
                val flatshareName = flatshareDocument.data?.get("name").toString()
                // Setting flatshare name on header
                findViewById<TextView>(R.id.roomshare_name).text = flatshareName
            }
            .addOnFailureListener { exception ->
                Log.d("Getting flatshare in getFlatshareNameInDatabase::MainActivity", "Error getting documents: ", exception)
            }
    }

    // Making visible fragments and navigation bar
    private fun showViews()
    {
        Handler().postDelayed({
            binding.container.visibility = View.VISIBLE
        }, 3000)
    }

    // Showing elements (views, fragments, navigation bar & flatshare name) on activity
    private fun showFlatshareElement()
    {
        this.setViews()
        this.showViews()
        this.getFlatshareNameInDatabase()
    }

    // Getting connected user in database
    private fun getUserInDatabase()
    {
        val username = firebaseAuth.currentUser?.displayName
        connectedUsername = username
        // Requesting database
        database
            .collection("users")
            .whereEqualTo("username", username)
            .limit(1)
            .get()
            .addOnSuccessListener { documents ->
                // User isn't in Firestore database statement
                if (documents.isEmpty)
                {
                    this.showFlatshareActivity()
                }
                // User is in Firestore database statement
                else
                {
                    for ( user in documents )
                    {
                        // Updating FCM token
                        this.updateToken(user.id)
                        // Initializing flatshare id
                        flatshareId = user.data["flatshare"].toString()
                        this.showFlatshareElement()
                    }

                }
            }
    }

    // Update FCM token
    private fun updateToken(userId : String)
    {
        // Getting new FCM token
        FirebaseMessaging.getInstance().token.addOnCompleteListener { task ->
            if (task.isSuccessful)
            {
                // Updating new token in database
                database
                    .collection("users")
                    .document(userId)
                    .update("token", task.result)
            }
        }
    }
}