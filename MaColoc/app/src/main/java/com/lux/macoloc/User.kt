package com.lux.macoloc

data class User
(
    val username : String?,
    val flatshare : String?,
    val token : String
)