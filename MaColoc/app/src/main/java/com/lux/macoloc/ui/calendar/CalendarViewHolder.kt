package com.lux.macoloc.ui.calendar

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.lux.macoloc.R
import com.lux.macoloc.ui.calendar.CalendarAdapter.OnItemListener
import java.time.LocalDate

class CalendarViewHolder(itemView: View, private val onItemListener: OnItemListener, private val days: ArrayList<LocalDate>) : RecyclerView.ViewHolder(itemView), View.OnClickListener
{
    val parentView: View = itemView.findViewById(R.id.parentView)
    val dayOfMonth: TextView = itemView.findViewById(R.id.cellDayText)

    override fun onClick(view: View)
    {
        onItemListener.onItemClick(adapterPosition, days[adapterPosition])
    }

    init
    {
        itemView.setOnClickListener(this)
    }
}