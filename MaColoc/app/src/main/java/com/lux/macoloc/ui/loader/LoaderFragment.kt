package com.lux.macoloc.ui.loader

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.lux.macoloc.R
import com.lux.macoloc.databinding.LoaderBinding


class LoaderFragment : Fragment()
{
    // Attributes.
    private var _binding: LoaderBinding? = null
//     This property is only valid between onCreateView and onDestroyView.
    private val binding get() = _binding!!


    // Fragment constructor method.
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Creating fragment
        _binding = LoaderBinding.inflate(inflater, container, false)

        this.navigateToTreasuryFragment()

        return binding.root
    }

    // Fragment's destroyer method.
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun navigateToTreasuryFragment()
    {
        findNavController().navigate(R.id.loader_to_treasury_fragment)
    }
}
