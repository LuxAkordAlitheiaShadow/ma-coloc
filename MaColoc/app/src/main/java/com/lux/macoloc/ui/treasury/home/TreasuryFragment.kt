package com.lux.macoloc.ui.treasury.home

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.lux.macoloc.MainActivity
import com.lux.macoloc.MainActivity.Companion.flatshareId
import com.lux.macoloc.R
import com.lux.macoloc.databinding.FragmentTreasuryBinding
import com.lux.macoloc.ui.treasury.Treasury

class TreasuryFragment : Fragment()
{
    // Attributes.
    private var _binding: FragmentTreasuryBinding? = null
    // This property is only valid between onCreateView and onDestroyView.
    private val binding get() = _binding!!

    val database = Firebase.firestore

    // Fragment constructor method.
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View
    {
        // Creating fragment
        _binding = FragmentTreasuryBinding.inflate(inflater, container, false)
        val root: View = binding.root

        if (flatshareId != "") // TODO make waiting coroutine
        {
            // Getting treasuries from database
            this.getTresuriesFromDB()
        }

        // Adding click event on add_treasury button
        this.addListenerFormButton()

        // Adding click event on view_balance button
        this.addListenerViewBalanceButton()

        return root
    }

    // Fragment's destroyer method.
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    // Methods.

    // Displaying treasury form fragment
    private fun showTreasuryFormFragment()
    {
        findNavController().navigate(R.id.fragment_treasury_to_fragment_treasury_form)
    }

    // Displaying treasury balance fragment
    private fun showTreasuryBalanceFragment()
    {
        findNavController().navigate(R.id.fragment_treasury_to_fragment_treasury_balance)
    }

    // Getting treasuries from database
    private fun getTresuriesFromDB()
    {
        val treasuryData = ArrayList<Treasury>()
        database
            .collection("flatshares")
            .document(flatshareId)
            .collection("treasuries")
            .orderBy("date", Query.Direction.DESCENDING)
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents)
                {
                    Log.d("Getting treasuries", "${document.id} => ${document.data}")
                    treasuryData.add(
                        Treasury
                        (
                            document.data["libelle"] as String,
                            document.data["picturePath"] as String?,
                            document.data["cost"] as Double,
                            document.data["date"] as String,
                            document.data["buyer"] as String,
                            document.data["stakeholders"] as List<String>,
                            document.data["stakeholdersCost"] as Map<String, Double>
                        )
                    )
                }
                this.displayTreasuries(treasuryData)
            }
            .addOnFailureListener { exception ->
                Log.w("Getting treasuries", "Error getting documents: ", exception)
            }
    }

    // Displaying treasuries to fragment
    private fun displayTreasuries( treasuryData : ArrayList<Treasury> )
    {
        // Getting the recycler view
        val recyclerTreasury = binding.recyclerTreasury
        recyclerTreasury.layoutManager = LinearLayoutManager(requireContext())

        // Setting data to recycler view
        val treasuryAdapter = RvCardAdapter(treasuryData)
        recyclerTreasury.adapter = treasuryAdapter
    }

    // Adding redirection event on add_treasury button
    private fun addListenerFormButton()
    {
        binding.addTreasury.setOnClickListener {
            Log.d("addTreasury", "Redirecting to treasury form fragment")
            this.showTreasuryFormFragment()
        }
    }

    // Adding redirection event on view_balance button
    private fun addListenerViewBalanceButton()
    {
        binding.viewBalanceButton.setOnClickListener {
            Log.d("View Balance", "Redirecting to treasury balance fragment")
            this.showTreasuryBalanceFragment()
        }
    }
}

