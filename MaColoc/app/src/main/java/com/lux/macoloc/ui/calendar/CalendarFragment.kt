package com.lux.macoloc.ui.calendar

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lux.macoloc.R
import com.lux.macoloc.databinding.FragmentCalendarBinding
import com.lux.macoloc.ui.calendar.CalendarAdapter.OnItemListener
import java.time.LocalDate


class CalendarFragment : Fragment(), OnItemListener {
    // Attributes.
    private var _binding: FragmentCalendarBinding? = null
    // This property is only valid between onCreateView and onDestroyView.
    private val binding get() = _binding!!

    private var monthYearText: TextView? = null
    private var calendarRecyclerView: RecyclerView? = null

    override fun onCreateView( inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        // Creating fragment
        _binding = FragmentCalendarBinding.inflate(inflater, container, false)
        val root: View = binding.root

        super.onCreate(savedInstanceState)
        initWidgets()
        CalendarUtils.selectedDate = LocalDate.now()
        setMonthView()
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val buttonWeeklyAction: Button = view.findViewById(R.id.buttonWeeklyAction)
        buttonWeeklyAction.setOnClickListener {
            findNavController().navigate(R.id.fragment_week_view)
        }
        val previousMonthAction: ImageButton = view.findViewById(R.id.previousMonthAction)
        previousMonthAction.setOnClickListener {
            previousMonthAction(view)
        }
        val nextMonthAction: ImageButton = view.findViewById(R.id.nextMonthAction)
        nextMonthAction.setOnClickListener {
            nextMonthAction(view)
        }
        val newEventAction: Button = view.findViewById(R.id.newEventAction)
        newEventAction.setOnClickListener {
            findNavController().navigate(R.id.fragment_calendar_event_edit)
        }
    }

    private fun initWidgets() {
        calendarRecyclerView = binding.calendarRecyclerView
        monthYearText = binding.monthYearTV
    }

    private fun setMonthView() {
        monthYearText?.text = CalendarUtils.monthYearFromDate(CalendarUtils.selectedDate)
        val daysInMonth = CalendarUtils.daysInMonthArray()
        val calendarAdapter = CalendarAdapter(daysInMonth, this)
        val layoutManager: RecyclerView.LayoutManager = GridLayoutManager(requireActivity().applicationContext, 7)
        calendarRecyclerView?.layoutManager = layoutManager
        calendarRecyclerView?.adapter = calendarAdapter
    }

    private fun previousMonthAction(view: View?) {
        CalendarUtils.selectedDate = CalendarUtils.selectedDate!!.minusMonths(1)
        setMonthView()
    }

    private fun nextMonthAction(view: View?) {
        CalendarUtils.selectedDate = CalendarUtils.selectedDate!!.plusMonths(1)
        setMonthView()
    }

    override fun onItemClick(position: Int, date: LocalDate?) {
        if (date != null) {
            CalendarUtils.selectedDate = date
            setMonthView()
        }
    }
}