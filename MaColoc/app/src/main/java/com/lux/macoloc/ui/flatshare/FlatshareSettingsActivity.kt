package com.lux.macoloc.ui.flatshare


import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.lux.macoloc.MainActivity
import com.lux.macoloc.R

class FlatshareSettingsActivity : AppCompatActivity()
{
    // Attributes.

    private val database = Firebase.firestore

    // Methods.

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_flatshare_setting)

        this.setFlatshareName()
        this.displayRoommates()
        this.setModifyFlatshareNameButton()
    }

    // Displaying treasuries to fragment
    private fun displayRoommates()
    {
        // Getting roommates from database
        val roommates : MutableList<String> = mutableListOf()
        database
            .collection("flatshares")
            .document(MainActivity.flatshareId)
            .collection("roommates")
            .get()
            .addOnSuccessListener { roommatesDocuments ->
                for ( roommate in roommatesDocuments )
                {
                    roommates.add(roommate.data["name"] as String)
                    // Getting the recycler view
                    val recyclerRoommate = findViewById<RecyclerView>(R.id.setting_recycler_roommates)
                    recyclerRoommate.layoutManager = LinearLayoutManager(this)
                    // Setting data to recycler view
                    val roommatesAdapter = RoommatesAdapter(roommates, this)
                    recyclerRoommate.adapter = roommatesAdapter
                }
            }
    }

    // Sending broadcast signal to finish Main Activity
    internal fun finishMainActivity()
    {
        val intent = Intent("finish_main_activity")
        sendBroadcast(intent)
    }

    // Launching flatshare setting activity
    internal fun showFlatshareActivity()
    {
        val intent = Intent(this, FlatshareActivity::class.java)
        startActivity(intent)
    }

    // Setting the flatshare name in Text View
    private fun setFlatshareName()
    {
        database
            .collection("flatshares")
            .document(MainActivity.flatshareId)
            .get()
            .addOnSuccessListener { flatshareDocument ->
                findViewById<TextView>(R.id.setting_flatshare_name).text = flatshareDocument.data!!["name"].toString()
            }
    }

    // Setting on click event listener on modify flatshare name button
    private fun setModifyFlatshareNameButton()
    {
        findViewById<Button>(R.id.modify_flatshare_name_button)
            .setOnClickListener {
                Toast.makeText(
                    this,
                    "Modification du nom de la colocation pas encore disponible.",
                    Toast.LENGTH_SHORT)
                    .show()
            }
    }
}