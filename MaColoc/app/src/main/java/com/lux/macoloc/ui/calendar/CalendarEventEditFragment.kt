package com.lux.macoloc.ui.calendar

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.RadioGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.lux.macoloc.R
import com.lux.macoloc.databinding.FragmentCalendarEventEditBinding
import java.time.LocalDate
import java.time.LocalTime
import android.view.LayoutInflater as LayoutInflater1


class CalendarEventEditFragment : Fragment() {

    // Attributes.
    private var _binding: FragmentCalendarEventEditBinding? = null
    // This property is only valid between onCreateView and onDestroyView.
    private val binding get() = _binding!!

    private lateinit var eventNameET: EditText
    private lateinit var eventDateTV: TextView
    private lateinit var eventTimeTV: TextView
    private lateinit var time: LocalTime

    override fun onCreateView(inflater: LayoutInflater1, container: ViewGroup?, savedInstanceState: Bundle?): View {
        // Creating fragment
        _binding = FragmentCalendarEventEditBinding.inflate(inflater, container, false)
        val root: View = binding.root

        super.onCreate(savedInstanceState)
        initWidgets()
        time = LocalTime.now()
        eventDateTV.setOnClickListener {
            val datePickerDialog = DatePickerDialog(
                requireContext(),
                { _, year, month, dayOfMonth ->
                    eventDateTV.text =   getString( R.string.edit_date, CalendarUtils.formattedDate(LocalDate.of(year, month+1, dayOfMonth)) )
                    CalendarUtils.selectedDate = LocalDate.of(year, month+1, dayOfMonth)

                },
                CalendarUtils.selectedDate!!.year,
                CalendarUtils.selectedDate!!.monthValue-1,
                CalendarUtils.selectedDate!!.dayOfMonth
            )
            datePickerDialog.show()
        }
        eventTimeTV.setOnClickListener {
            val timePickerDialog = TimePickerDialog.OnTimeSetListener {timePicker, hour, minutes ->
                eventTimeTV.text = getString(R.string.edit_time, CalendarUtils.formattedShortTime(LocalTime.of(hour, minutes)) )
                time = LocalTime.of(hour, minutes)
            }
            TimePickerDialog(requireContext(), timePickerDialog, time.hour, time.minute, true).show()
        }
        eventDateTV.text = getString(R.string.edit_date, CalendarUtils.formattedDate(CalendarUtils.selectedDate))
        eventTimeTV.text = getString(R.string.edit_time, CalendarUtils.formattedShortTime(LocalTime.of(time.hour, time.minute)))

         return root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var categoryRB: String = ""
        val eventRG: RadioGroup = view.findViewById(R.id.radioGroupTypeEvent)

        // Thjis is to select the Radio Button for event, taking the category to set in DB
        eventRG.setOnCheckedChangeListener { group: RadioGroup, checkedId -> // checkedId is the RadioButton selected
            when (checkedId) {
                R.id.lessonRB -> {
                    categoryRB = "Cours"
                }
                R.id.workRB -> {
                    categoryRB = "Travail"
                }
                R.id.partyRB -> {
                    categoryRB = "Soirée"
                }
                R.id.eventRB -> {
                    categoryRB = "Évènement"
                }
                R.id.otherRB -> {
                    categoryRB = "Autres"
                }
            }
        }
        // Save Button for the event
        val saveEventAction: Button = view.findViewById(R.id.saveEventAction)
        saveEventAction.setOnClickListener {
            saveEventAction(view, categoryRB)
            Log.i("INFO", "Save Event OK")
        }
    }

    private fun initWidgets() {
        eventNameET = binding.eventNameET
        eventDateTV = binding.eventDateTV
        eventTimeTV = binding.eventTimeTV
    }

    private fun saveEventAction(view: View?, category: String) {
        val eventName = eventNameET.text.toString()
        val eventDate = CalendarUtils.selectedDate
        val newEvent = CalendarEvent(eventName, eventDate, time)
        newEvent.addEventToBDD(eventName, CalendarUtils.formattedShortDate(eventDate), CalendarUtils.formattedShortTime(time), category)
        findNavController().navigate(R.id.fragment_calendar_saveEventEdit)
    }
}