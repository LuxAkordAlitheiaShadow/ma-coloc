package com.lux.macoloc.ui.calendar

import android.util.Log
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.lux.macoloc.MainActivity
import java.time.LocalDate
import java.time.LocalTime


class CalendarEvent(var name: String, var date: LocalDate?, var time: LocalTime?) {
    val database = Firebase.firestore

    fun addEventToBDD (name: String, date: String, time: String, category:String) {
        database
            .collection("flatshares")
            .document(MainActivity.flatshareId)
            .collection("calendarEvent")
            .add(
                Calendar(
                    name,
                    date,
                    time,
                    category
                )
            )
            .addOnSuccessListener { documentReference ->
                Log.d("Add calendarEvent to DB", "Calendar added with name : $documentReference.came")
            }
            .addOnFailureListener { e ->
                Log.d("Add calendarEvent to DB", "Error adding calendarEvent to DB", e)
            }

    }


}