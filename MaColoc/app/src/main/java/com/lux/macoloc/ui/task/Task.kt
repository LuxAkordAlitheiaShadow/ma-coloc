package com.lux.macoloc.ui.task

data class Task
(
    val name: String,
    val person: List<String>
)