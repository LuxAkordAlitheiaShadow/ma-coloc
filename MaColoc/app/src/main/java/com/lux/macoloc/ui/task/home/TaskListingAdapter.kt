package com.lux.macoloc.ui.task.home

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.lux.macoloc.MainActivity
import com.lux.macoloc.R
import com.lux.macoloc.ui.task.Task
import org.json.JSONException
import org.json.JSONObject


class TaskListingAdapter(private val cardList: ArrayList<Task>, private val activity: MainActivity) : RecyclerView.Adapter<TaskListingAdapter.TaskViewHolder>()
{
    val database = Firebase.firestore


    private val FCM_API = "https://fcm.googleapis.com/fcm/send"
    private val requestQueue: RequestQueue by lazy {
        Volley.newRequestQueue(activity.applicationContext)
    }

    // Class containing elements of treasury card layout
    class TaskViewHolder(view: View) : RecyclerView.ViewHolder(view)
    {
        val nameView : TextView = view.findViewById(R.id.name_task_card)
        val personView : TextView = view.findViewById(R.id.name_task_person_card)
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int) : TaskViewHolder
    {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.task_listing_card, viewGroup, false)

        return TaskViewHolder(view)
    }

    private fun deleteTaskButton(delete_task_button: ImageButton, event: Task) {
        delete_task_button.tag = event.name
    }

    private fun deleteEvent(delete_task_button: ImageButton) {
        database
            .collection("flatshares")
            .document(MainActivity.flatshareId)
            .collection("tasks")
            .whereEqualTo("name", delete_task_button.tag)
            .get()
            //.delete()
            .addOnSuccessListener {
                    documents -> Log.d("MAJ", "Task deleted")
                for (event in documents) {
                    database
                        .collection("flatshares")
                        .document(MainActivity.flatshareId)
                        .collection("tasks")
                        .document(event.id)
                        .delete()
                        .addOnSuccessListener { Log.d("MAJ", "Task deleted")}
                        .addOnFailureListener { exception -> Log.d("INFO", "Error deleting documents: ", exception)}
                }
            }
            .addOnFailureListener { exception -> Log.d("INFO", "Id Document not found: ", exception)
            }
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: TaskViewHolder, position: Int)
    {
        val deleteTaskButton: ImageButton = viewHolder.itemView.findViewById(R.id.delete_task_button)
        val alertTaskButton: ImageButton = viewHolder.itemView.findViewById(R.id.alert_task_button)

        // Get element from your dataset at this position and replace the
        // contents of the view with that element

        var personViewText : String = ""
        for ( roommate in cardList[position].person)
        {
            personViewText += " $roommate,"
        }
        personViewText = personViewText.drop(1)
        personViewText = personViewText.dropLast(1)

        viewHolder.personView.text = personViewText
        viewHolder.nameView.text = cardList[position].name //Get task name

        deleteTaskButton(deleteTaskButton, cardList[position])
        deleteTaskButton.setOnClickListener {
            deleteEvent(deleteTaskButton)
            cardList.removeAt(position)
            notifyItemRemoved(position)
        }

        alertTaskButton.setOnClickListener {
            this.getRoommateTokenFromDatabase(cardList[position].person, cardList[position].name)
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() : Int
    {
        return cardList.size
    }

    private fun getRoommateTokenFromDatabase(roommatesToNotify : List<String>, taskName : String)
    {
        for (roommateToNotify in roommatesToNotify)
        {
            database
                .collection("users")
                .whereEqualTo("username", roommateToNotify)
                .limit(1)
                .get()
                .addOnSuccessListener { roommateDocument ->
                    for (roommate in roommateDocument)
                    {
                        val roommateToken : String = roommate.data["token"] as String
                        this.sendNotification(roommateToken, taskName)
                    }
                }
                .addOnFailureListener { exception ->
                    Log.d("Getting user in getRoommateTokenFromDatabase::TaskListingAdapter", "Error getting documents: ", exception)
                }
        }
    }

    private fun sendNotification(tokenToNotify : String, taskName: String)
    {
        val notification = JSONObject()
        val notificationBody = JSONObject()

        try
        {
            notificationBody.put("title", "Tache demandee")
            notificationBody.put("message", "Tu peux-tu faire la tache $taskName ?")   //Enter your notification message
            notification.put("to", tokenToNotify)
            notification.put("data", notificationBody)
        }
        catch (e: JSONException)
        {
            Log.e("sendNotification", "Error on create notification " + e.message)
        }

        val jsonObjectRequest = object : JsonObjectRequest(FCM_API, notification, Response.Listener<JSONObject>
        { response ->
            Log.i("sendNotification", "onResponse: $response")
        },
            Response.ErrorListener
            {
                Toast.makeText(activity.applicationContext, "Impossible d'envoyer une notification", Toast.LENGTH_LONG).show()
                Log.i("TAG", "onErrorResponse: Didn't work")
            })
        {
            override fun getHeaders(): Map<String, String>
            {
                val params = HashMap<String, String>()
                params["Authorization"] = "key=" + "AAAAJr-eJ14:APA91bFsdlLQu1H5Y5ec9IHphoC95ALwMhhJ6E_iAnY9CNvIVeMaiPJrF8N6eVql1igWAxEuYnSERp0Aybrjg9f6NYS0vL44GlYdLTSYNz35U0rwJQ6eT4g9K4bCJCv17UjzRA2VAM8I"
                params["Content-Type"] = "application/json"
                return params
            }
        }
        requestQueue.add(jsonObjectRequest)
        Toast.makeText(activity.applicationContext, "La notification est envoye", Toast.LENGTH_SHORT).show()
    }

}