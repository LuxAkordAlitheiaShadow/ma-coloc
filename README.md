# Ma Coloc - README FR

## Presentation

Ma coloc est une application de gestion interne de colocation/foyer.

## Fonctionnalités

### Connexion

La connexion a l'application se fait grace a Google Authenticator.
Ce dernier permet à l'utilisateur de se connecter avec son compte Google ou bien avec un couple email-mot de passe.

### Créer ou rejoindre une colocation

Apres connexion, et si l'utilisateur n'est pas rattaché à une application, l'utilisateur est amené sur la fenêtre de selection/creation de colocation.

Si l'utilisateur veut créer une nouvelle colocation, il lui suffit de renseigner le nom de sa colocation puis de cliquer sur créer.
Il sera ensuite redirigé vers l'interface principale de l'application et de son foyer.

Si l'utilisateur veut rejoindre une colocation existante, il lui suffit de copier le code de colocation partageable dans le champ prévu a cet effet de cliquer sur rejoindre.
Si le code est erroné, un message d'avertissement est affiche.

Dans un cas comme dans l'autre, si l'utilisateur clique sur un bouton sans que le champ requis soit rempli, un message d'avertissement est affiche.

### Paramètres

Dans l'entête de l'application, à droite du nom de la colocation, figure un bouton paramètres, symbolise par un engrenage.
En cliquant dessus, un menu s'affiche permettant plusieurs fonctionnalités.

#### Déconnexion

Simple déconnexion vous ramenant a la page de connexion de Google Authenticator.

#### Partage du code de colocation

Pour permettre à d'autres utilisateurs de rejoindre la colocation, cette fonctionnalité vous permet de partager votre code de colocation au travers d'application de partage (SMS, Facebook, Discord, ...).

#### Paramètres de la colocation

Cette fenêtre vous permet de visualiser les colocataires present dans la colocation et de les expulser de l'instance de la colocation.
L'expulsion retire l'utilisateur de la base de donnees.
Un message s'affiche à l'expulsion pour indiquer la balance de la fonctionnalité trésorerie que ce colocataire expulse avait.

### Trésorerie

La trésorerie est la fonctionnalité permettant de renseigner des frais partagés entre vos colocataires / membres du meme foyer.
Au travers de celle-ci vous pouvez visualiser tous les frais partages, en ajouter des nouvelles et établir une balance entre les parties prenantes de toutes les transactions.

#### L'ajout d'un frai

Depuis la fenêtre de la fonctionnalité de trésorerie, vous montrant les frais partages existants, vous pouvez ajouter une nouvelle via le bouton plus.
Ce bouton va vous amenez sur une nouvelle fenêtre présentant un formulaire pour les renseignements de votre frai partage.

- Le nom
- Le cout
- La date ( Par défaut la date du jour )
- Une image en guise de preuve ( Optionnel )
- L'acheteur ( Par défaut l'utilisateur connecté )
- Les parties prenantes

Si l'un des champs non optionnels n'est pas rempli, un pop-up ( Toast ) d'alerte vous le mentionnera à la tentative d'ajout.

À la tentative d'ajout, la transaction est enregistrée en base de donnees sous la collection `treasuries` du document de la colocation.

#### La balance des frais

Également depuis la fenêtre de la fonctionnalité de trésorerie, vous pouvez accéder à la fenêtre des balances en appuyant sur le bouton graphique.

Sur cette nouvelle fenêtre, vous pouvez visualiser la repartition des frais partages par colocataire.
**Attention !** Pour des raisons d'économie de bande passante et de sollicitation a la base de donnees, la synchronisation ne se fait pas automatique à toutes les ouvertures de cette fenêtre.
Il faut appuyer sur le bouton de synchronisation en bas de cette page pour mettre à jour la balance localement et en base de donnees.

### Calendrier

*Redaction en cours*

### Taches ménagères

*Redaction en cours*

## Fonctionnalités à développer

- Accueil
    - Flux d'actualité de la colocation
- Paramètres
    - Partage par code QR
    - Modification du nom de la colocation
- Paramètres du colocataire
    - Modification des informations utilisateurs
    - Choix de la langue de l'application
- Covoiturage
    - Recensement des voitures de la colocation et de leur disponibilité
    - Ajout d'un voyage avec kilométrage et parties prenantes
    - Lien automatique des frais d'essence sur la trésorerie
- Messagerie
- Tutoriel/Aide d'utilisation
- Theme clair/sombre
- Menus de la semaine
    - Liste de course
    - Proposition des menus de la semaine
- Gestion de plusieurs colocations par utilisateur
- Trésorerie
    - Gestion de balance a l'expulsion
    - Modification et suppression d'un frai
    - Reconnaissance d'un ticket de caisse
    - Sélecteur d'éléments d'un ticket de caisse par colocataire
    - Selection de devise
    - Notification
    - Calcul d'équilibrage de la balance ( qui doit combien a qui )
- Calendrier
    - Organisation d'un évènement temporaire pour des utilisateurs qui sont hors de la colocation
    - Lien avec l'application calendrier d'Android
- Taches ménagères
    - Ajout d'un membre temporaire aux taches ménagères

## Technologies utilisées

- Android Studio Bumblebee IDE
- Kotlin  211-1.6.20
- Android API 26 (Android 8.0)
- Google Firebase
- Gradle 7.1.3
